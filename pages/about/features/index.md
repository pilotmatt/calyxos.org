---
title: Key Features
---

**Verified Boot:** to prevent tampering with the phone's operating system, using cryptographic signatures

**Security and privacy by default:** Instead of the bloatware, spyware, and backdoors installed by default in many versions of Android, CalyxOS includes a suite of privacy, security, and censorship circumvention apps available out of the box, including encrypted phone calls and texts with Signal, anonymous web browsing with Tor, advertising, tracker blocking and private search using DuckDuckGo Privacy Browser, encrypted email on your phone using K-9 Email client with OpenKeyChain and Yubikey support, free VPN service from Calyx and from Riseup.net and Universal Two Factor authentication using Yubikey hardware tokens.

**Android without spyware:** CalyxOS has reconfigured Android to avoid Google's spyware and tracking. It uses the F-droid app store for installing and updating free software applications, and strips out Google's privacy invasive business model, replacing it with microG, a free software re-implementation of Google’s proprietary services which allows you to run nearly every app from Google's Play Store without giving Google access to your phone.

**Regular security updates and patches:** CalyxOS receives the latest security updates for Android, fixing software vulnerabilities and security flaws that can put your phone at risk.
