---
title: Development
---

CalyxOS is Free and Open Source Software.

All development of CalyxOS takes place out in the open on public source code repositories. We would love to have you get involved.

Our source code is available for review at [https://gitlab.com/calyxos](https://gitlab.com/calyxos).

You can get started by following our HOWTO for [creating a CalxOS development environment](https://gitlab.com/calyxos/calyxos/wikis/Getting-Started).

Then, check out [list of open issues](https://gitlab.com/calyxos/calyxos/issues/).
